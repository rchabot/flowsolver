(require "ltk" "../src/ltk-0.98/ltk.lisp")
(require "creation" "../src/creation-grille.lisp")
(require "primitives" "../src/primitives.lisp")
(require "resolution" "../src/algo-resolution.lisp")
(require "affichage" "../src/affichage-ltk.lisp")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; La fonction résolution prend plusieurs arguments:    ;;
;;   -La grille à résoudre                              ;;
;; Les arguments suivants sont optionnels               ;;
;;   -La technique de résolution:                       ;;
;;      NIL -> parcours profondeur basique              ;;
;;      1 -> parcours avec vérifier-conditions          ;;
;;   -L'affichage voulu par l'utilisateur:              ;;
;;      NIL -> résolution sans affichage                ;;
;;      1 -> affichage de la résolution coup par coup   ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;



(defparameter G (creer-grille))

(charger-grille "../grille/6x6-0.txt" G)

(time (resolution G 1 nil))
(affichage-grille G)




(require "ltk" "../src/ltk-0.98/ltk.lisp")
(require "creation" "../src/creation-grille.lisp")
(require "primitives" "../src/primitives.lisp")
(require "resolution" "../src/algo-resolution.lisp")
(require "affichage" "../src/affichage-ltk.lisp")
(require "tests-primitives" "../tests/test-primitives.lisp")
(require "tests-resolution" "../tests/test-resolution.lisp")

(defparameter G (creer-grille))

(test-primitives G)
(test-resolution G)
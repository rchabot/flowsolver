Dans cette partie, nous allons détailler les stratégies utilisées pour résoudre la grille. Les exemples abordés par la suite utilisent une grille carrée de taille constante $4\times 4$ ou $5 \times 5$ dans un souci de clarté, bien que notre programme fonctionne pour des grilles de taille $n*m$ avec $(n,m) \in \mathbb{N}$.

\section{Contrôle de la validité}

Tout d'abord, commençons par nous attaquer aux problèmes \textsc{Validité} et \textsc{Chemin-correct} définis dans la partie \ref{subsec:regles}.

Le premier problème à traiter est \textsc{Chemin-correct} car celui-ci va nous permettre d'apporter une solution au problème \textsc{Validité}.
Pour cela, nous avons écrit une fonction \texttt{chemin-correct} définie telle que:\\

\begin{algorithm}
  \Entree{Grille G, couleur}
  \Sortie{Le booléen \textsc{vrai} si le chemin est correctement formé pour la couleur}
  \Si{couleur $\neq 0$}{
  encours $\leftarrow$ \texttt{trouver-depart(G,couleur)}\\
  arrivee $\leftarrow$ \texttt{trouver-arrivee(G,couleur)}
  
  \Tq{encours $\neq$ nil}{
    encours $\leftarrow$ \texttt{case-suivante(G,encours)}\\
    \Si{encours = arrivee}{
      \Retour{\textsc{Vrai}}}
    }
  \Retour{\textsc{Faux}}
  }
  \caption{Fonction \texttt{chemin-correct}}
  \label{chemin-correct}
\end{algorithm}

\begin{algorithm}[h!]
  \Entree{Grille G}
  \Sortie{Le booléen \textsc{Vrai} si la grille est valide}
  \Si{G.nb-cases-vide $\neq 0$}{
    nb-chemins-ok $\leftarrow 0$\\
    2voisins $\leftarrow$ \textsc{Vrai}\\
    \Pour{k de 1 à G.nb-couleurs}{
      \Si{\texttt{chemin-correct(G,k)}}{nb-chemins-ok++}}
    \Pour{i de 1 à G.dimensions[0]}{
      \Pour{j de 1 à G.dimensions[1]}{
        \Si{case(i,j) $\neq$ \texttt{trouver-depart(G,couleur)} \textbf{ou} case(i,j) $\neq$ \texttt{trouver-arrivee(G,couleur)}}{
          \Si{nb-acces(case(i,j)) $\neq 2$}{
            2voisins $\leftarrow$ \textsc{Faux}}}}}
    \eSi{nb-chemins-ok = G.nb-couleurs \textbf{et} 2voisins = \textsc{Vrai}}
    {\Retour{\textsc{Vrai}}}
    {{\Retour{\textsc{Faux}}}}
    }
  \caption{Fonction \texttt{est-resolue}}
  \label{est-resolue}
\end{algorithm}


Dans cet algorithme, la fonction \texttt{case-suivante(G, case)} renvoie la case suivante dans le chemin étudié si elle existe, la valeur $nil$ sinon. La fonction \texttt{chemin-correct} permet d'avoir une écriture simple de \texttt{est-resolue} apportant une solution au problème \textsc{Validité}. Cette fonction est détaillée dans l'algorithme \ref{est-resolue}.\\

L'algorithme \ref{est-resolue} consiste à vérifier que toutes les cases de la grille sont coloriées, et que chacune des couleurs de la grille possède un chemin correct dont toutes les cases ont au moins deux voisins (sauf les cases d'arrivée et de départ du chemin). On peut noter que cet algorithme a une complexité $\mathcal{O}(n*m)$, ce qui correspond à une complexité linéaire étant donné que la grille en entrée est de taille $n*m$ également.

\newpage
\section{Stratégie de résolution simple}

\subsection{Description}

Maintenant que nous sommes capables de décider si une grille est résolue ou non, nous pouvons aborder le problème \textsc{Résoudre} et tenter d'y apporter une solution. 
La solution basique que nous proposons ici est d'effectuer un parcours en profondeur de la grille pour chacune des couleurs, jusqu'à ce que la grille soit résolue. L'objectif est de rallier, pour chaque couleur, la case d'arrivée du chemin à partir de la case de départ. Pour cela nous avons choisi d'utiliser la version itérative du parcours en profondeur, en s'appuyant sur une pile d'appel. Les éléments de la pile d'appel sont définis par la structure suivante:\\

\noindent \textbf{Structure:} \texttt{chemin} \\
\indent \texttt{case :} case \\
\indent \texttt{précédente :} case \\

Cette structure permet de mémoriser dans un élément de la pile la case en train d'être visitée ainsi que la case qui lui a été précédemment visitée. Lorsqu'une case de départ est empilée, on impose la convention:

\begin{center}
\begin{tabular}{|c|c|}
  \hline
  case de départ (x,y) & G.case-nulle (-1,-1)\\
  \hline
\end{tabular}
\end{center}

Illustrons la méthode utilisée ici sur un exemple, en affichant dans la pile la couleur de la case dans la grille.\\

On commence par initialiser la pile en empilant la case de départ.

\input{step1}

A partir de là, on stocke dans la pile les cases accessibles à partir de la case située en tête de pile. Les cases que l'on ne visite pas restent vide, et on colorie la case qui sera visitée. Ainsi, sur la figure \ref{fig:parcours-pro} on voit que la case (0,3) est dans un premier temps non visitée, donc stockée dans la pile. On choisit de visiter plutôt la case (1,2). Et on continue l'exploration jusqu'à atteindre la case d'arrivée.

\input{step2}

La parcours en profondeur trouve un premier chemin, celui-ci est valide mais la grille ne pourra pas être résolue car les cases (0,3), (1,3) et (2,3) restent vides et inaccessibles dans cette configuration. Ainsi nous allons chercher un second chemin correct. Pour cela, on dépile jusqu'à ce l'on tombe sur une case vide dans la pile (ou jusqu'à ce qu'il y ait égalité du champ \texttt{précédente} de deux éléments successifs de la pile).

\input{step3}

Cette fois-ci, le chemin construit finit dans une impasse (tête du chemin en (2,1)), on revient donc en arrière en dépilant afin de trouver un nouveau chemin correct. Cette fois, on parvient à un chemin correct. On itère le procédé pour les autres couleurs de la grille. Au final, on obtient la pile de la figure \ref{fig:pile}.

\input{step4}


\subsection{Fonctions requises}

Afin de réaliser le parcours en profondeur comme illustré ci-dessus, nous avons besoin de plusieurs fonctions dont nous allons décrire les spécifications.

Tout d'abord à chaque étape, il est nécessaire de déterminer l'ensemble des cases accessibles à partir de la case située en tête de la pile.
Nous avons donc une première fonction:\\

\noindent \textbf{Fonction:} \texttt{cases-adjacentes-libres} \\
\indent \textbf{E:} Case C\\
\indent \textbf{S:} La liste des cases accessibles à C de telle sorte que si une case d'arrivée est trouvée, elle est positionnée en fin de liste\\

Mettre la case d'arrivée pour la couleur de la case C explorée est capital. Si l'on ne le fait pas, cela peut entraîner le fait que des chemins corrects explorés soient considérés comme non-trouvés, du fait de la structure des deux algorithmes qui vont suivre.

Ensuite, afin d'empiler les cases et de colorier et vider la grille, nous avons deux grandes fonctions, qui affectent la grille et la pile par effet de bords dans l'idéal. En pratique, nous avons procédé par copie de la pile, cela nous étant apparu comme plus simple à implémenter vu nos connaissances en Common Lisp. Ces deux fonctions sont décrites par les algorithmes \ref{rebrousser-chemin} et \ref{construire-chemin}:

\begin{algorithm}
  \label{rebrousser-chemin}
  \caption{Procédure \texttt{rebrousser-chemin}}
  \Entree{Grille G, pile non-vide P}
  prec $\leftarrow$ (tete(P).precedente)\\
  \texttt{vider-case(tete(P).case)}\\
  depiler(P)\\
  \Tq{!est-vide(P) \textbf{et} prec $\neq$ (tete(P).precedente)}{
    prec $\leftarrow$ (tete(P).precedente)\\
    \texttt{vider-case(tete(P).case)}\\
  depiler(P)\\
  }
  \texttt{colorier-case(tete(P).case, tete(P).precedente.couleur)}
\end{algorithm}

Cette procédure permet donc de dépiler la pile jusqu'à trouver une case non-explorée. Les cases de départ et d'arrivée sont dépilées comme toute autre case, sauf qu'on ne permet pas à la fonction \texttt{vider-case} de vider ces cases spécifiques (c'est-à-dire, d'enlever leur couleur). Si toutes les cases ont été explorées, la fonction vide entièrement la pile (c'est ce qui se produirait en appliquant \texttt{rebrousser-chemin} à la pile de la figure \ref{fig:pile}).
 On peut noter que la ligne 8 permet de colorier directement la case jusqu'à laquelle on est redescendu dans la pile. \\

La procédure \texttt{construire-chemin} décrite par l'algorithme \ref{construire-chemin} permet d'empiler les éléments de la pile jusqu'à ce qu'un chemin correct soit trouvé. En effet, la fonction rebrousse chemin par elle-même lorsqu'on arrive dans un cul-de-sac (cf. figure \ref{fig:sac}). On peut remarquer que lorsqu'un chemin n'est pas trouvé pour une couleur, \texttt{construire-chemin} permet de revenir automatiquement à l'exploration pour la couleur précédente afin de trouver un nouveau chemin correct si celui-ci existe.

La pile n'étant initialisée dans aucune de ces deux fonctions, nous avons également besoin d'une fonction \texttt{empiler-depart} qui permet d'empiler la case de départ du chemin pour une couleur donnée, ceci afin de savoir quel chemin est exploré lorsque la pile est passée en paramètre de la fonction \texttt{construire-chemin}.

\newpage
\begin{algorithm}[h!]
  \label{construire-chemin}
  \caption{Procédure \texttt{construire-chemin}}
  \Entree{Grille G, pile non-vide P}
  encours $\leftarrow$ tete(P)\\
  prec $\leftarrow$ encours.precedente\\
  couleur $\leftarrow$ encours.case.couleur\\
  arrivee $\leftarrow$ \texttt{trouver-arrivee(G,couleur)}\\
  \Tq{!est-vide(P) \textbf{et} encours.case $<>$ arrivee}{
    L $\leftarrow$ \texttt{cases-adjacentes-libres(G,encours.case)}\\
    \eSi{est-vide(L)}{
      \texttt{rebrousser-chemin(G,P)}\\
      \Si{!est-vide(P)}{
        couleur $\leftarrow$ encours.case.couleur\\
        arrivee $\leftarrow$ \texttt{trouver-arrivee(G,couleur)}\\}}{
      \Pour{i de 1 à longueur(L)}{
        prec $\leftarrow$ encours.case\\
        empiler(P, (L[i],prec))\\}
      \texttt{colorier-case(tete(P).case)}}
  encours $\leftarrow$ tete(P)}
  
\end{algorithm}


\subsection{Algorithme de résolution}

Les deux fonctions \texttt{construire-chemin} et \texttt{rebrousser-chemin} désormais bien définies, nous pouvons écrire l'algorithme de parcours en profondeur nous permettant de résoudre une grille lorsque cela est possible:

\begin{algorithm}[h!]
  \Entree{Grille G}
  \Sortie{Grille résolue si une solution a été trouvée}
  pile $\leftarrow$ PileVide() \\
  couleur $\leftarrow$ Choisir-couleur() \\
  pile $\leftarrow$ \texttt{empiler-depart(pile,couleur)}\\
  \Tq{!est-resolue(G) \textbf{et }!est-vide(pile)}{
    \texttt{construire-chemin(G,pile)}\\
    couleur $\leftarrow$ tete(pile).case.couleur\\
    \eSi{est-vide(pile)}{\Retour{\textsc{Grille non solvable}}}{
      \Si{Un chemin correct a été trouvé}{
        \Si{couleur = dernière couleur}{
          \Si{!est-resolue(G)}{
            \texttt{rebrousser-chemin(G,pile)}\\
            couleur $\leftarrow$ tete(pile).case.couleur
            }}
          \Sinon{couleur $\leftarrow$ Choix-couleur-suivante()\\
            pile $\leftarrow$ \texttt{empiler-depart(pile,couleur)}}}
      \Sinon{
            \texttt{rebrousser-chemin(G,pile)}\\              
            couleur $\leftarrow$ tete(pile).case.couleur\\
      }
    }
  }
  \caption{Fonction \texttt{resoudre}}
\end{algorithm}

Lorsque nous avons implémenté cette fonction, la fonction $Choisir-couleur()$ est la plus basique possible et consiste simplement à incrémenter la variable couleur. Néanmoins des choix plus complexes peuvent être réalisés, notamment avec les frontières, on choisira de préférence une couleur dont le départ et l'arrivée se situent sur un bord de la grille. De même pour la fonction $Choix-couleur-suivante()$.

\subsection{Performances\label{subsec:perf}}

Le principe de cet algorithme étant d'effectuer un parcours en profondeur, en supposant que dans le pire des cas l'algorithme parcourt la totalité de la grille sans trouver de solution, on peut évaluer la complexité de \texttt{resoudre} en $\mathcal{O}(3^{n*m})$ où $n*m$ sont les dimensions de la grille. En effet, dans le pire des cas, chaque case peut atteindre 3 nouvelles cases dans la construction du chemin. On a donc ici un algorithme de complexité exponentielle. \\

Pour évaluer ses performances, nous l'avons exécuté sur des grilles 4x4, 5x5, 6x6, 7x7 et 8x8. Nous avons également effectué les tests sur 2 machines différentes:

\begin{itemize}
        \item Machine 1: machine de l'école. Processeur Intel Core 2 Duo 2.93 GHz. Architecture 32 bits.
        \item Machine 2: machine personnelle. Processeur Intel Core i7 2.2GHz. Architecture 64bits.
\end{itemize}        

\noindent Nous avons pu mesurer les temps d'exécution moyens suivants, en utilisant la fonction \texttt{time} de Common Lisp:\\

\begin{figure}[h!]
  \centering
  \begin{tabular}{|c|c|c|c|c|c|c|c|}
    \hline
    Grille & 5x5 & 6x6 & 7x7 & 8x8-0 & 8x8-1 & 8x8-2 & 9x9-0 \\
    \hline
    Machine 1 & 0.1s & 3.74s & 3min49s & $>$15min & 4min6s & $>$15min & $>$15min \\
    \hline
    Machine 2 & 0,64s & 1.9s & 2min31s  & $>$15min & 4min4s & $>$15min & $>$15min\\ 
   \hline
  \end{tabular}
  \caption{Mesures des performances de l'algorithme de parcours en profondeur}
\end{figure}

On remarque qu'à partir d'une grille de taille 7x7, le temps de résolution devient long (supérieur à 6 minutes parfois). Ce qui s'explique par la complexité exponentielle de l'algorithme. Néanmoins, pour la grille \texttt{8x8-1.txt} nous constatons qu'il est possible de trouver rapidement une solution alors qu'aucune solution n'est trouvée en moins de 15 min pour les 2 autres grilles. Nous interprétons ce résultat de la manière suivante:\\

La vitesse de résolution de la grille dépend du conditionnement de celle-ci. Lorsqu'elle est lue par le parseur, une rotation au niveau des numéros des couleurs attribuées dans la grille peut influencer de manière flagrante le temps de résolution de celle-ci. Cela s'explique par le fait que nous parcourons les couleurs successivement les unes après les autres. C'est pourquoi nous avons intégré les fonctions \texttt{choisir-couleur} et \texttt{choix-couleur-suivante} dans l'écriture de l'algorithme de résolution, ces fonctions pouvant permettre d'optimiser le choix des couleurs en fonction d'un remplissage de la grille donné. Malheureusement, nous n'avons pas eu le temps de nous focaliser sur ce point.\\
Nous avons tout de même trouvé d'autres stratégies afin d'améliorer la vitesse d'exécution de l'algorithme, celles-ci pouvant être elles aussi améliorées avec un choix optimisé pour l'ordre des couleurs à visiter. 

\section{Stratégies de résolution avancées\label{subsec:strategie}}

\subsection{Utilisation de certaines contraintes}

Lorsque nous avons essayé de résoudre ce jeu par nous-même, nous avons pu noter certaines contraintes que nous pouvions nous imposer afin de résoudre une grille. Nous allons ici présenter quelques unes d'entre elles, que nous avons implémenté afin de réduire le temps d'exécution de \texttt{construire-chemin} en particulier.  

\input{cas1}

Par exemple sur la figure \ref{fig:cas11}, on voit que la case (0,3) n'a plus qu'un seul accès possible, aucun chemin correct ne pourra donc être formé en intégrant cette case. Il est nécessaire pour chaque case de garder toujours 2 accès disponibles, sauf pour les cases de départ et d'arrivée qui doivent n'avoir qu'un seul accès. De plus cela permettra d'éviter de parcourir inutilement un certains nombres de chemins possibles. On peut alors caractériser une fonction \texttt{vérifier-conditions} de la manière suivante:\\

\begin{itemize}
  \item Chaque case de départ et d'arrivée doit toujours avoir au moins un accès disponible. Une case adjacente coloriée de la même couleur et étant la case suivante (resp. précédente) dans le chemin vis-à-vis du départ (resp. de l'arrivée) est considérée comme un accès.
  \item Toute autre case doit toujours avoir au moins 2 accès disponibles, la dernière case construite pour un chemin étant considérée comme accessible, et une case appartenant à un chemin étant considérée comme ayant 2 accès (une case précédente et une case suivante).\\
\end{itemize}

Sur la figure \ref{fig:cas12}, en supposant qu'on est en train de construire le chemin vert, on a à l'instant de la figure:\\

\begin{itemize}
  \item \texttt{nb-acces((0,2))} $=2$
  \item \texttt{nb-acces((0,1))} $=2$
  \item \texttt{nb-acces((2,1))} $=1$
  \item \texttt{nb-acces((2,2))} $=4$
  \item \texttt{nb-acces((1,2))} $=3$\\
\end{itemize}

La fonction \texttt{verifier-conditons} peut être alors intégrée à l'algorithme \ref{construire-chemin} au niveau de la ligne 16, en ajoutant un appel à \texttt{rebrousser-chemin} lorsque les conditions que nous venons d'évoquer ne sont pas vérifiées. La complexité de l'algorithme de résolution reste alors exponentielle mais cela permet d'accélérer la résolution dans certains cas. Nous évaluerons les nouvelles performances de l'algorithme dans la section \ref{subsec:perf}.


\subsection{Utilisation des frontières\label{subsec:frontiere}}

Nous avons également remarqué qu'une utilisation des frontières de la grille pouvait permettre d'accélérer la résolution.
C'est pourquoi nous avons ajouté un champ \texttt{bord} à la structure case. Le principe d'utilisation des frontières est le suivant: 

Si le point de départ et le point d'arrivée appartiennent tous les deux au bord de la grille, sans qu'un point de départ ou d'arrivée d'une autre couleur ne se situe entre eux, alors on construit le chemin permettant de relier ces deux points en suivant le bord de la grille. Le bord de la grille est défini par les cases ayant leur champ \texttt{bord} égal à 1. Les cases coloriées une fois le chemin construit définissent de nouvelles frontières pour la grille en mettant le champ \texttt{bord} à 2. Cette nouvelle définition des frontières impose d'évaluer les nouvelles bordures de la grille afin d'itérer le procédé. Illustrons notre propos sur un exemple.

\input{cas2}


On voit sur la grille gauche de la figure \ref{fig:cas2} que les points de départ et d'arrivée des couleurs jaunes et verts sont en bordure de la grille. On construit donc les 2 chemins corrects en suivant le bord de la grille comme le montre la grille de droite. 

Ce coloriage implique un changement dans la définition des bords de la grille. Les cases des chemins jaune et vert deviennent des frontières de la grille. La nouvelle bordure de la grille est donc désormais composée des cases [(1,1), (1,2), (1,3), (2,3), (3,3), (4,3), (4,2), (4,1), (3,1), (2,1)]. On remarque alors que les points de départ et d'arrivée de la couleur orange appartiennent tous les deux à la bordure de la grille. On construit donc le chemin orange en suivant les bords comme illustré dans la figure \ref{fig:cas3}. Pour finir, on répète le procédé une dernière fois avec la couleur bleu et on voit que sur cet exemple le remplissage par les bords s'avère suffisant pour résoudre la grille.

\input{cas3}

\subsection{Performances}

Pour valider ces nouvelles stratégies de résolution, de nouveaux tests doivent être menés sur les mêmes grilles et les mêmes machines que dans la section \ref{subsec:perf}. Néanmoins nous tenons à signaler que nous avons seulement eu la possibilité d'implémenter la stratégie intégrant la fonction \texttt{verifier-conditions}. Aucune implémentation de résolution en utilisant la stratégie des frontières n'a été réalisée. 

En outre, les tests menés ont montré que notre fonction \texttt{verifier-conditions} bugge, déterminant la grille non-solvable alors qu'une solution avait été trouvée au préalable (cf. grille 6x6-2, 6x6-4, 7x7-3, 7x7-4 et autres). Mais pour montrer que cette fonction demeure efficace en théorie, nous illustrons son efficacité sur la grille \texttt{8x8-1.txt} et quelques grilles 7x7. On constate notamment pour  que \texttt{8x8-1.txt} la résolution se fait en moins de 5 minutes alors qu'elle était supérieure à 15 minutes auparavant.


\begin{figure}[h!]
  \centering
  \begin{tabular}{|c|c|c|c|c|c|c|c|}
    \hline
    Grille & 5x5 & 6x6 & 7x7 & 8x8-0 & 8x8-1 & 8x8-2 & 9x9-0 \\
    \hline
    Machine 1 & 0.1s & 3.74s & 31,9s & 4min16 & 1min49 & $>$15min & $>$15min \\
    \hline
    Machine 2 & 0.1s & 0.72s & 20.4s  & 2min27s & 1min9 & $>$15min & $>$15min\\ 
   \hline
  \end{tabular}
  \caption{Mesures des performances de l'algorithme}
\end{figure}


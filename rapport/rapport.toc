\select@language {french}
\contentsline {chapter}{Introduction}{2}
\contentsline {chapter}{\numberline {1}D\IeC {\'e}finition des probl\IeC {\`e}mes et mod\IeC {\'e}lisation des donn\IeC {\'e}es}{3}
\contentsline {section}{\numberline {1.1}D\IeC {\'e}finition des probl\IeC {\`e}mes}{3}
\contentsline {subsection}{\numberline {1.1.1}R\IeC {\`e}gles du jeu}{3}
\contentsline {subsection}{\numberline {1.1.2}R\IeC {\'e}solution de la grille}{4}
\contentsline {section}{\numberline {1.2}Mod\IeC {\'e}lisation des donn\IeC {\'e}es}{4}
\contentsline {subsection}{\numberline {1.2.1}Structure des cases}{4}
\contentsline {subsection}{\numberline {1.2.2}Structure de la grille}{5}
\contentsline {subsection}{\numberline {1.2.3}Manipulation du fichier texte}{5}
\contentsline {section}{\numberline {1.3}Interface graphique}{6}
\contentsline {chapter}{\numberline {2}R\IeC {\'e}solution de la grille}{7}
\contentsline {section}{\numberline {2.1}Contr\IeC {\^o}le de la validit\IeC {\'e}}{7}
\contentsline {section}{\numberline {2.2}Strat\IeC {\'e}gie de r\IeC {\'e}solution simple}{8}
\contentsline {subsection}{\numberline {2.2.1}Description}{8}
\contentsline {subsection}{\numberline {2.2.2}Fonctions requises}{10}
\contentsline {subsection}{\numberline {2.2.3}Algorithme de r\IeC {\'e}solution}{11}
\contentsline {subsection}{\numberline {2.2.4}Performances}{12}
\contentsline {section}{\numberline {2.3}Strat\IeC {\'e}gies de r\IeC {\'e}solution avanc\IeC {\'e}es}{12}
\contentsline {subsection}{\numberline {2.3.1}Utilisation de certaines contraintes}{12}
\contentsline {subsection}{\numberline {2.3.2}Utilisation des fronti\IeC {\`e}res}{13}
\contentsline {subsection}{\numberline {2.3.3}Performances}{14}
\contentsline {chapter}{Conclusion}{15}

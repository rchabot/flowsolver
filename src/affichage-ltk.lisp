(require "grille" "../src/creation-grille.lisp")
(require "primitives" "../src/primitives.lisp")
(require "resolution" "../src/algo-resolution.lisp")
(require "ltk" "../src/ltk-0.98/ltk.lisp")
(provide "affichage")

;Liste des couleurs dispos
(defparameter LC '(black green pink red purple orange yellow grey blue brown))

(defun cellule (G i j LC C)
  "Fonction qui affiche une case de la grille"
  (let ((couleur (couleur G j i))
	(rectangle (ltk:create-rectangle C
					 (+ 10 (* 50 i))
					 (+ 10 (* 50 j))
					 (+ 50 (* 50 i))
					 (+ 50 (* 50 j)))))
    (ltk:itemconfigure C rectangle 'fill (nth couleur LC))))
 

(defun afficher(G)
  (ltk:with-ltk ()
    (let* ((sc (make-instance 'ltk:scrolled-canvas))
	   (c (ltk:canvas sc)))
      
      (let ((M (grille-matrice g)))
	(dotimes (y (array-dimension M 1))
	  (dotimes (x (array-dimension M 0))
	    (cellule G y x LC c))))
      (ltk:pack sc :expand 1 :fill :both)
      (ltk:scrollregion c 0 0 800 800))))
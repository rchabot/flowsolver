(require "grille" "../src/creation-grille.lisp")
(require "primitives" "../src/primitives.lisp")
(provide "resolution")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Structure des éléments utilisés dans la pile pour résolution ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(defstruct chemin
  zone
  precedente)


(defun affichage-pile (pile)
  (mapcar (lambda (elt)
	    (format t "Case: (~D,~D) | Couleur: ~D | Précédente: (~D,~D) ~%"
		    (zone-coord-i (chemin-zone elt))
		    (zone-coord-j (chemin-zone elt))
		    (zone-couleur (chemin-zone elt))
		    (zone-coord-i (chemin-precedente elt))
		    (zone-coord-j (chemin-precedente elt))))
	  pile))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Fonctions pour la résolution de la grille ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(defun est-complete (G)
  "Fonction vérifiant si toutes les cases de la grille sont coloriées"
  (nb-cases-vides G)
  (not (/= 0 (grille-nb-cases-vides G))))


(defun est-obstacle (G i j)
  "Obstacle = case coloriée"
  (not (zerop (couleur G i j))))


(defun chemin-correct (G couleur)
  "Fonction qui décide si le chemin formé pour la couleur entrée en paramètre est valide ou non"
  (unless (zerop couleur)
    (let ((en-cours (trouver-depart G couleur))
	  (arr (trouver-arrivee G couleur)))
      (loop
	 (setq en-cours (case-suivante G (zone-coord-i en-cours) 
				       (zone-coord-j en-cours)))
	 (cond ((null en-cours) 
		(return nil))
	       ((eq arr (case-suivante G (zone-coord-i en-cours) 
				       (zone-coord-j en-cours)))
		(return T)))))))
  

(defun est-resolue (G)
  "Fonction qui décide si une grille est résolue ou non"
  (if (not (est-complete G))
      NIL
      (let ((nb-chemins-OK 0)
	    (A (grille-matrice G))
	    (2voisins T))
	(loop 
	   for k from 1 to (grille-nb-couleurs G) 
	   do 
	     (when (chemin-correct G k) 
	       (incf nb-chemins-OK)))
	
	(dotimes (i (array-dimension A 0))
	  (dotimes (j (array-dimension A 1))
	    (when (and (not (eq (obtenir-case G i j) 
				(trouver-depart G (couleur G i j))))
		       (not (eq (obtenir-case G i j) 
				(trouver-arrivee G (couleur G i j)))))
	      (unless (eq (nb-acces G i j) 2)
		(setf 2voisins NIL)))))
	
	(when (and (= nb-chemins-OK (grille-nb-couleurs G)) 
		   (not (null 2voisins)))
	  T))))



(defun cases-adjacentes-libres (G zone prec)
  "Renvoie la liste des cases adjacentes à zone non coloriées/qui ne sont pas la case précédente"
  (remove-if (lambda (z)
	       (or (null z)
		   (and  (not (equal (trouver-arrivee 
				      G 
				      (zone-couleur zone)) z))
			 (est-obstacle G 
				       (zone-coord-i z) 
				       (zone-coord-j z)))
		   (eq z prec))) 
	     (cases-adjacentes G 
			       (zone-coord-i zone) 
			       (zone-coord-j zone))))

(defun appartient-liste (liste elt)
  (unless (null liste)
    (let ((bool nil))
      (dotimes (i (list-length liste))
	(when (eq (nth i liste) elt)
	  (setf bool T)))
      bool)))


(defun ordonner-liste (G liste zone)
  (unless (null liste)
    (let ((arr (trouver-arrivee G (zone-couleur zone))))
      (when (appartient-liste liste arr)
	(setf liste (remove-if (lambda (elt)
				 (eq elt arr))
			       liste))
	(setf liste (reverse (push arr liste))))))
  liste)



(defun verifier-conditions (G)
  (let ((n (array-dimension (grille-matrice G) 0))
	(m (array-dimension (grille-matrice G) 1))
	(bool t))
	    
    (dotimes (i n)
      (dotimes (j m)
	(let ((couleur (couleur G i j)))
	  (cond
	    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	    ;; Cas où (G i j) est un départ ou une arrivée et n'a pas d'acces ;;
	    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	    ((and (or (eq (obtenir-case G i j) (trouver-depart G couleur))
		      (eq (obtenir-case G i j) (trouver-arrivee G couleur)))
		  (not (chemin-correct G couleur))
		  (zerop (nb-acces G i j)))
	     (setf bool nil))

	    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	    ;; Cas où (G i j) est une case coloriée et a moins de 2 acces ;;
	    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	    ((and (not (zerop couleur))
		  (and (not (eq (obtenir-case G i j) (trouver-depart G couleur)))
		       (not (eq (obtenir-case G i j) (trouver-arrivee G couleur))))
		  (> 2 (nb-acces G i j))
		  (not (chemin-correct G couleur)))
	     (setf bool nil))

	    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	    ;; Cas où (G i j) est une case non coloriée et a moins de 2 acces ;;
	    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	    ((and (zerop couleur)
		  (> 2 (nb-acces G i j)))
	     (setf bool nil))
	    
	    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	    ;; Sinon les conditions sont vérifiées ;;
	    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	    (t bool)))))
    bool))




(defun rebrousser-chemin (G pile)
  "Fonction qui supprime des éléments de la pile et en vide les cases"
  (unless (null pile)
    (let ((prec (chemin-precedente (car pile)))
	  (arr (trouver-arrivee G (zone-couleur (chemin-zone (car pile))))))
      
      (vider-case G 
		  (zone-coord-i (chemin-zone (car pile)))
		  (zone-coord-j (chemin-zone (car pile))))
      (pop pile)
      
      (loop 
	 while (and (not (null pile))
		    (not (eq (chemin-precedente (car pile)) 
			     prec)))
	 do 
	   ;(print "depilage")
	   (setf prec (chemin-precedente (car pile)))
	   (vider-case G 
		       (zone-coord-i (chemin-zone (car pile)))
		       (zone-coord-j (chemin-zone (car pile))))
	   (pop pile)))
    
    (unless (null pile)
      (colorier-case G 
		     (zone-coord-i (chemin-zone (car pile))) 
		     (zone-coord-j (chemin-zone (car pile))) 
		     (zone-couleur (chemin-precedente (car pile))))))
  pile)




(defun construire-chemin (G pile &optional option affichage)
  "Fonction qui empile des cases et en colorie certaines"
  (unless (null pile)
    (let ((encours (car pile))
	  (prec (chemin-precedente (car pile)))
	  (couleur (zone-couleur (chemin-zone (car pile))))
	  (arrivee (trouver-arrivee G (zone-couleur (chemin-zone (car pile))))))
    
      (loop 
	 while (and (not (null pile))
		    (not (equal (chemin-zone encours) arrivee)))
	   
	 do 
	   (let ((L (ordonner-liste G
				    (cases-adjacentes-libres 
				     G	
				     (chemin-zone encours) 
				     (chemin-precedente encours))
				    (chemin-zone encours))))
	     (cond ((null L)  
  		   ;(print "Liste nulle: Rebrousser-Chemin")
		    (setf pile (rebrousser-chemin G pile))
		    (unless (null pile)
		      ; Cas où on dépile jusqu'à revenir au chemin d'une couleur antérieure
		      (setf couleur (zone-couleur (chemin-zone 
						   (car pile))))
		      (setf arrivee (trouver-arrivee 
				     G 
				     (zone-couleur 
				      (chemin-zone (car pile)))))))
		   (t
		    ;(print "Parcours en cours")
		    (dotimes (i (list-length L))
		      (setf prec (chemin-zone encours))
		      (push (make-chemin :zone (nth i L)
					 :precedente  prec) 
			    pile))
		    (colorier-case G 
				   (zone-coord-i (chemin-zone (car pile)))
				   (zone-coord-j (chemin-zone (car pile)))
				   couleur)
		    (unless (null option)
		      (when (not (verifier-conditions G))
			;(print "Non Conditions")
			(setf pile (rebrousser-chemin G pile))
			(unless (null pile)
			  (setf couleur (zone-couleur (chemin-zone 
						       (car pile))))
			  (setf arrivee (trouver-arrivee 
					 G 
					 (zone-couleur 
					  (chemin-zone (car pile))))))))))
	     
	     (unless (null pile)
	       (setf encours (car pile)))
	    ;(affichage-grille G)
	    ;(affichage-pile pile)
	     (unless (null affichage)
	       (let ((M (grille-matrice g)))
		 (dotimes (y (array-dimension M 1))
		   (dotimes (x (array-dimension M 0))
		     (cellule G y x LC affichage))))))))
    pile))


(defun empiler-depart (G couleur pile)
  "Empile la case départ d'une couleur pour démarrer le parcours en profondeur dans la pile"
  (when (<= couleur (grille-nb-couleurs G))
    (let ((depart (trouver-depart G couleur)))
      (push (make-chemin :zone depart
			 :precedente (grille-case-nulle G))
	    pile)))
  pile)



(defun resoudre (G &optional option affichage)
  "Algorithme de résolution d'une grille"
  (print "Grille en cours de résolution")
  (let* ((sc nil)
	 (c nil))
    (unless (null affichage)
      (setf sc (make-instance 'ltk:scrolled-canvas))
      (setf c (ltk:canvas sc)))
    
    (let ((pile nil)
	  (couleur 1))
    
      (setf pile (empiler-depart G couleur pile))
      (loop
       while (and (not (est-resolue G))
		  (not (null pile)))
	 
       do
	 (setf pile (construire-chemin G pile option c))
	 (unless (null affichage)
	   (let ((M (grille-matrice g)))
	     (dotimes (y (array-dimension M 1))
	       (dotimes (x (array-dimension M 0))
		 (cellule G y x LC c))))
	   (ltk:pack sc :expand 1 :fill :both)
	   (ltk:scrollregion c 0 0 800 800))
	 (unless (null pile)
	   (setf couleur (zone-couleur (chemin-zone (car pile)))))
	 
	 (cond 
	   ((null pile)
	    (print "La grille est non-solvable")
	    (return NIL))
	   
	   ((eq (chemin-zone (car pile)) (trouver-arrivee G couleur)) 
	    ;(print "Chemin trouvé")
	    (when (< couleur (+ (grille-nb-couleurs G) 1)) 
	      ;(print "Passer à couleur suivante")
	      (setf couleur (+ couleur 1))
	      (setf pile (empiler-depart G couleur pile)))
	    

	    (when (eq couleur (+ (grille-nb-couleurs G) 1))
	      ;(print "Dernière couleur")
	      (unless (est-resolue G)
		(setf pile (rebrousser-chemin G pile))
		(unless (null pile)
		  (setf couleur (zone-couleur (chemin-zone 
					       (car pile))))))))
	   
	   ((not (eq (zone-couleur (chemin-zone (car pile))) couleur))
	    ;(print "Chemin non trouvé")
	    (setf pile (rebrousser-chemin G pile))
	    (unless (null pile)
	      (setf couleur (zone-couleur (chemin-zone (car pile)))))))))

  (est-resolue G)))



(defun resolution (G &optional option affichage)
 "Résout la grille et affiche en temps réel en option"
 (cond ((not (null affichage))
	 (ltk:with-ltk ()
	     (resoudre G option affichage)))
	(t (resoudre G option))))

(provide "grille")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Définition des structures utilisées et relatives à la grille ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defstruct grille
  dimensions
  matrice
  nb-couleurs 
  nb-cases-vides
  case-nulle)

(defstruct zone
  coord-i
  coord-j
  couleur
  bord
  depart
  position)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Fonctions de création de la grille ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(defun creer-grille ()
  "Création de la grille de jeu"
  (make-grille :dimensions '(0 0)
	       :nb-couleurs 0
	       :case-nulle (make-zone :coord-i -1
				      :coord-j -1
				      :couleur -1
				      :bord -1
				      :depart 2
				      :position -1)))


(defun init-grille-matrice (g)
  "Initialise tous les éléments de la matrice comme étant des structures zone ne contenant rien"
  (let ((M (grille-matrice g)))
    (dotimes (i (array-dimension M 0))
      (dotimes (j (array-dimension M 1))
        (setf (aref M i j) (make-zone :coord-i i
				      :coord-j j
				      :couleur nil
				      :bord nil
				      :depart 0
				      :position 0))
	(if (or (zerop i) 
		(zerop j) 
		(= i (- (array-dimension M 0) 1)) 
		(= j (- (array-dimension M 1) 1)))
	    (setf (zone-bord (aref M i j)) 1)
	    (setf (zone-bord (aref M i j)) 0))))))


(defun init-position (G)
  "Initialise les positions des départs et arrivées après création de la grille"
  (let ((M (grille-matrice g)))
    (dotimes (i (array-dimension M 0))
      (dotimes (j (array-dimension M 1))
	(when (eq (zone-depart (aref M i j)) 1)
	  (setf (zone-position (aref M i j)) 1))
	(when (eq (zone-depart (aref M i j)) -1)
	  (setf (zone-position (aref M i j)) 2))))))


(defun affichage-grille (G)
  "Permet un affichage basique de la grille"
  (let ((M (grille-matrice g))
	(liste nil))
    (dotimes (i (array-dimension M 0))
      (dotimes (j (array-dimension M 1))
	(push (zone-couleur (aref M i j)) liste))
      (setf liste (reverse liste))
      (print liste)
      (setf liste nil))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Fonctions pour le parsing du fichier et permettant de générer la grille ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(defun read-file-lines (f)
  "Lit le fichier texte ligne par ligne et stocke ces lignes dans une liste de chaînes de caractères"
  (with-open-file (in f :direction :input)
    (let ((res nil))
      (do ((line (read-line in nil) (read-line in nil)))
          ((null line) (nreverse res))
        (when (char/= (char line 0) #\-)
	  (push line res))))))



(defun recup-dim (f grille)
  "Stocke dans la structure grille les dimensions du jeu"
  (let ((l (read-file-lines f)))
    (multiple-value-bind (x n) (parse-integer (car l) :junk-allowed t) 
      (let ((y (parse-integer (car l) :start 
			      (+ 1 (cadr (list x n))) :junk-allowed t)))
	(setf (grille-dimensions grille) (list x y))))))


(defun test-in (e l)
  "Teste l'appartenance d'un élément dans une liste"
  (cond ((null l) nil)
	((= e (car l)) t)
	(t (test-in e (cdr l)))))


(defun charger-grille (f grille)
  "Stocke dans la matrice les caractéristiques de chaque zone"
  (recup-dim f grille)
  (let ((l (cdr (read-file-lines f)))
	(x (car (grille-dimensions grille)))
	(y (cadr (grille-dimensions grille)))
	(list nil))
    (setf (grille-matrice grille) 
	  (make-array (grille-dimensions grille) :initial-element nil))
    (init-grille-matrice grille)
    (let ((M (grille-matrice grille)))
      (dotimes (i x)
	(dotimes (j y)
	  (cond ((char= (char (car l) j) #\#)
		 (setf (zone-couleur (aref M i j)) 0))
		(t 
		 (let ((coul (parse-integer 
			      (car l) :start j :end (+ 1 j) :junk-allowed t)))
		   (setf (zone-couleur (aref M i j)) coul)
		   (cond ((test-in coul list)
			  (setf (zone-depart (aref M i j)) -1))
			 (t
			  (setf (zone-depart (aref M i j)) 1)
			  (push coul list)))))))
	(setf l (cdr l)))
      (setf (grille-nb-couleurs grille) (length list))
      (setf (grille-nb-cases-vides grille) 
	    (- (* x y) (* 2 (grille-nb-couleurs grille))))))
  (init-position G))



(require "grille" "../../src/creation-grille.lisp")
(require "primitives" "../../src/primitives.lisp")
(require "conditions" "../../src/conditions.lisp")
(require "est-resolue" "../../src/est-resolue.lisp")

(compile-file "ltk")
(load "ltk")
(in-package :ltk)
(ltk::ltk-eyes)


(defparameter o '("#111" "4555" "4444" "1111"))

;tranformons la chaine de caractere en liste: "#111"=>(#111) pour ensuite recuperer la car
(defun transform (l)
  (let ((string l))
    (loop :for (integer position) := (multiple-value-list 
				      (parse-integer string
						     :start (or position 0)
						     :junk-allowed t))
       :while integer
       :collect integer)))

;test
(transform "2 3 4")
;=>(2 3 4) parfait !

 
;liste des couleur dispo
(defparameter LC '('black 'green 'pink 'red 'black 'orange 'yellow 'grey))

;recuperer une couleur de la liste

;TEST retourner red
(nth 2 LC)

;fonction qui retourne la couleur choisie
(defun nieme (n l)
  (if (> n 7)
      nil
      (nth (1- n) l)))

;test
(nieme 2 LC)
;=>PINK

(defun affichage ()
  (with-ltk()
    (let* ((gcanevas (make-instance 'canvas
				    :width 1000
				    :height 1000))
	   (grille (canvas gcanevas)))
      ;afficher l'ensemble des cases en blanches
      (affiche-grille grille 9 10 'green)
      
      ;(afficher-case grille 2 3 'blue)
      
      
      ;afficher une case avec une couleur bien précise
	;(afficher-case grille 1 2 'red) 
  
      ;(text (create-text grille 230 456 "flow"))
      (pack gcanevas :expand 1 :fill :both))))


  
  
(affichage)




;afficher le rectangle (la cellule)
(defun cellule (grille i j couleur)
  (let (;(text (create-text grille 90 600 "Software designed by: "))
	;(text (create-text grille 90 620 "C.THIERRY - A.PATRY - R.CHABOT - A.DOHGMI"))
	(rectangle( create-rectangle grille
				     (+ 10 (* 50 i))
				     (+ 10 (* 50 j))
				     (+ 50 (* 50 i))
				     (+ 50 (* 50 j)))))
    (itemconfigure grille rectangle 'fill couleur)))

   
    
   

(defun affiche-colonne (grille i j couleur)
  (cellule grille i (1- j) couleur)
  (if (> j 0)
      (affiche-colonne grille i (1- j) couleur)))

(defun affiche-grille (grille i j couleur)
  (affiche-colonne grille (1- i) j couleur)
  (if (> i 0)
      (affiche-grille grille (1- i) j couleur)))




;souris
(defun afficher-case(grille i j couleur)
    "colorier une case i j avec une couleur"
    (let* ((gcanevas (make-instance 'ltk:canvas
				    :width 50
				    :height 50)))
      (ltk:place gcanevas (* i 50) (* j 50))
      (ltk:itemconfigure gcanevas (ltk:create-rectangle gcanevas 5 5 45 45) 'fill couleur )
      (ltk:itemconfigure gcanevas (ltk:create-rectangle gcanevas 5 5 45 45) 'outline couleur)
      ;on clique pour rajouter une couleur
      (ltk:bind gcanevas "<ButtonPress-1>"
                (lambda (evt)
                  (if (coloration-possible new-board (list x y) (number-color picked-color))
                      (draw-case (board-change-coord-dynamique new-board
                                                               (number-color picked-color)
                                                               (list x y))
                                 x y picked-color))))
      ;on clique pour retirer la couleur
      (ltk:bind f "<ButtonPress-3>"
                (lambda (evt)
                  (draw-case (remove-position-dynamique new-board
                                                        (list x y)) 
                             x y 'grey90)))))
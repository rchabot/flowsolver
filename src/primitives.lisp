(require "grille" "../src/creation-grille.lisp")
(provide "primitives")


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Fonctions d'accès aux champs des structures ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(defun obtenir-case (G i j)
  (aref (grille-matrice G) i j))

(defun affecter-couleur (G i j couleur)
  (setf (zone-couleur (aref (grille-matrice G) i j)) couleur))

(defun couleur (G i j)
  (zone-couleur (aref (grille-matrice G) i j)))

(defun affecter-bordure (G i j bord)
  (setf (zone-bord (aref (grille-matrice G) i j)) bord))

(defun bordure (G i j)
  (zone-bord (aref (grille-matrice G) i j)))

(defun affecter-depart (G i j dep)
  (setf (zone-depart (aref (grille-matrice G) i j)) dep))

(defun est-depart (G i j)
  (eq (zone-depart (aref (grille-matrice G) i j)) 1))
  
(defun est-arrivee (G i j)
  (eq (zone-depart (aref (grille-matrice G) i j)) -1))

(defun est-case (G i j)
  (eq (zone-depart (aref (grille-matrice G) i j)) 0))

(defun affecter-position (G i j pos)
  (setf (zone-position (aref (grille-matrice G) i j)) pos))

(defun quelle-position (G i j)
  (zone-position (aref (grille-matrice G) i j)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Fonctions pour obtenir les cases adjacentes à la case visitée ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(defun case-gauche (G i j)
  (let ((n (array-dimension (grille-matrice G) 0))
	(m (array-dimension (grille-matrice G) 1)))
    (when (and (> j 0) (< j m) (>= i 0) (< i n)) 
      (aref (grille-matrice G) i (- j 1)))))


(defun case-droite (G i j)
  (let ((n (array-dimension (grille-matrice G) 0))
	(m (array-dimension (grille-matrice G) 1)))
    (when (and (>= j 0) (< j (- m 1)) (>= i 0) (< i n)) 
      (aref (grille-matrice G) i (+ j 1)))))


(defun case-haut (G i j)
  (let ((n (array-dimension (grille-matrice G) 0))
	(m (array-dimension (grille-matrice G) 1)))
    (when (and (>= j 0) (< j m) (> i 0) (< i n)) 
      (aref (grille-matrice G) (- i 1) j))))


(defun case-bas (G i j)
  (let ((n (array-dimension (grille-matrice G) 0))
	(m (array-dimension (grille-matrice G) 1)))
    (when (and (>= j 0) (< j m) (>= i 0) (< i (- n 1)))
      (aref (grille-matrice G) (+ i 1) j))))


(defun cases-adjacentes (G i j)
  "Renvoie la liste des 4 cases qui sont adjacentes à la case passée en paramètre dans l'ordre (Gauche Droite Haut Bas). S'il n'y a pas de case gauche par exemple, gauche vaut NIL."
  (let ((n (array-dimension (grille-matrice G) 0))
	(m (array-dimension (grille-matrice G) 1)))
    (when (and (>= i 0) (< i n) (>= j 0) (< j m))
      (list (case-gauche G i j) (case-droite G i j) 
	    (case-haut G i j) (case-bas G i j)))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Fonctions pour trouver les points de départ et ;;
;; d'arrivée pour la couleur donnée               ;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(defun trouver-depart (G couleur)
  "Renvoie la case de départ pour la couleur passée en paramètre"
  (unless (zerop couleur)
    (let ((A (grille-matrice g))
	  (n (array-dimension (grille-matrice G) 0))
	  (m (array-dimension (grille-matrice G) 1)))
      
      (defun boucle (i m)
	(dotimes (j m)
	  (when (and (eq (zone-couleur (aref A i j)) couleur) 
		     (eq (zone-depart (aref A i j)) 1)) 
	    (return (aref A i j)))))
      
      (defun double-boucle (n m)
	(dotimes (i n)
	  (unless (null (boucle i m)) 
	    (return (boucle i m)))))
      
      (double-boucle n m))))
  

(defun trouver-arrivee (G couleur)
  "Renvoie la case d'arrivée pour la couleur passée en paramètre"
  (unless (zerop couleur)
    (let ((A (grille-matrice g))
	  (n (array-dimension (grille-matrice G) 0))
	  (m (array-dimension (grille-matrice G) 1)))
      
      (defun boucle (i m)
	(dotimes (j m)
	  (when (and (eq (zone-couleur (aref A i j)) couleur) 
		     (eq (zone-depart (aref A i j)) -1)) 
	    (return (aref A i j)))))
      
      (defun double-boucle (n m)
	(dotimes (i n)
	  (unless (null (boucle i m)) 
	    (return (boucle i m)))))
      
      (double-boucle n m))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Fonctions de coloriage de la grille ;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(defun colorier-case (G i j couleur)
  "Fonction qui associe une couleur donnée à une case de la grille"
  (when (zerop (couleur G i j))
    (let ((arr (trouver-arrivee G couleur)))
      (affecter-couleur G i j couleur)
      (affecter-position G i j (zone-position arr))
      (affecter-position G (zone-coord-i arr) (zone-coord-j arr) 
			 (1+ (zone-position arr))))))


(defun vider-case (G i j)
  "Fonction qui remet une case à zéro"
  (let ((couleur (couleur G i j)))
    (unless (zerop couleur)
      (let ((arr (trouver-arrivee G couleur))
	    (dep (trouver-depart G couleur)))
	(unless (or (eq arr (obtenir-case G i j))
		    (eq dep (obtenir-case G i j)))
	  (affecter-couleur G i j 0)
	  (affecter-position G i j 0)
	  (affecter-position G (zone-coord-i arr) (zone-coord-j arr) 
			     (1- (zone-position arr))))))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;   Autres fonctionnalités   ;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(defun case-suivante (G i j)
  "Renvoie la case suivante dans un chemin colorié"
  (let ((liste (remove-if #'null (cases-adjacentes G i j)))
	(A (grille-matrice G))
	(pos 0)
	(couleur (couleur G i j)))
    
    (when (and (not (zerop (zone-position (aref A i j))))
	       (not (zerop couleur)))
      
      (setf pos (zone-position (aref A i j)))
      (dotimes (k (length liste))
	(let ((elt (nth k liste)))
	  (when (and (eq (zone-couleur elt) couleur) 
		     (= (+ pos 1) (zone-position elt))) 
	    (return elt)))))))


(defun case-precedente (G i j)
  "Renvoie la case précédente dans un chemin colorié"
  (let ((liste (remove-if #'null (cases-adjacentes G i j)))
	(A (grille-matrice G))
	(pos 0)
	(couleur (couleur G i j)))
    
    (when (and (not (zerop (zone-position (aref A i j))))
	       (not (zerop couleur)))
      
      (setf pos (zone-position (aref A i j)))
      (dotimes (k (length liste))
	(let ((elt (nth k liste)))
	  (when (and (= (zone-couleur elt) couleur) 
		     (= (- pos 1) (zone-position elt)))
	    (return elt)))))))


(defun est-dernier-construit (G i j)
  "Regarde si la case est la dernière d'un chemin en train d'être construit"
  (let ((couleur (couleur G i j))
	(pos-case (quelle-position G i j))
	(pos-arr 0)
	(arr nil))
    
    (when (and (not (zerop couleur))
	       (not (eq (trouver-depart G couleur) (obtenir-case G i j))))
      
      (setf arr (trouver-arrivee G couleur))
      (setf pos-arr (quelle-position G (zone-coord-i arr) 
				     (zone-coord-j arr))))
    
    (and (= pos-arr (+ pos-case 1))
	 (not (eq (case-precedente G (zone-coord-i arr) 
				   (zone-coord-j arr)) 
		  (obtenir-case G i j))))))



(defun nb-acces (G i j)
  "Fonction donnant le nombre d'acces de la case i j"
  (let ((couleur (couleur G i j))
	(adj (remove-if #'null (cases-adjacentes G i j)))
	(N-acces 0)
	(dep-ou-arr-adj 0)
	(construit-adj 0)
	(construit-couleur 0))
    
    (when (not (null adj))
      (dotimes (n 2)
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;; On parcourt 2 fois la liste, le premier parcours permettant           ;;
	;; de savoir si un chemin est en cours de construction autour de la case ;;
	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	(dotimes (k (list-length adj))
	  (let ((elt (nth k adj)))
	    (cond
	      ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	      ;; Cas où la case adjacente n'est pas un obstacle ;;
	      ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	      ((and (not (est-obstacle G (zone-coord-i elt) (zone-coord-j elt)))
		    (zerop n))
	       (incf N-acces))
	      
	      ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	      ;; Cas où une case-adjacente est la dernière case d'un chemin en train d'être construit ;;
	      ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	      ((and (est-dernier-construit G 
					   (zone-coord-i elt) 
					   (zone-coord-j elt))
		    (zerop n))
	       (setf construit-couleur (zone-couleur elt))
	       (incf construit-adj)
	       (incf N-acces))

	      ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	      ;; Cas où une case-adjacente est coloriée de la même couleur ;;
	      ;; que (G i j) et est la case précédente ou la case suivante ;;
	      ;; de (G i j) dans le chemin pour la couleur.                ;;
	      ;; On a alors 2 acces quand la case appartient au chemin     ;;
	      ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	      ((and (zerop n)
		    (or (eq (obtenir-case G i j) (case-suivante G 
								(zone-coord-i elt) 
								(zone-coord-j elt)))
			(eq (obtenir-case G i j) (case-precedente G 
								  (zone-coord-i elt) 
								  (zone-coord-j elt)))))
	       (incf N-acces))

	      ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	      ;; Cas où peut accéder à une case arrivée ou départ d'une autre couleur ;;
	      ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	      ((and (zerop couleur)
		    (zerop construit-couleur)
		    (not (eq (zone-couleur elt) construit-couleur))
		    (zerop dep-ou-arr-adj)
		    (or (= (zone-depart elt) 1) (= (zone-depart elt) -1))
		    (= n 1))
	       ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	       ;; On regarde le tour d'avant si on a pas un chemin en cours          ;;
	       ;; de construction qui est adjacent. Car si on ne vérifie pas,        ;;
	       ;; on calcule 2 acces possibles alors que le chemin vient d'arriver     ;;
	       ;; dans un cul de sac avec pour cases adjacentes un depart ou arrivée ;;
	       ;; d'une autre couleur que celle recherchée                           ;;
	       ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	       (incf dep-ou-arr-adj)
	       (incf N-acces))

	      ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	      ;; Cas où (G i j) est le maillon manquant pour terminer     ;;
	      ;; la construction d'un chemin (on a alors 2 acces minimum) ;;
	      ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	      ((and (= n 1)
		    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
		    ;; On regarde au 1er tour si on a un chemin ;;
		    ;; construit qui est adjacent               ;;
		    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
		    (not (zerop construit-couleur))
		    ;;;;;;;;;;;;
		    ;; Si oui ;;
		    ;;;;;;;;;;;;
		    (and (or (= (zone-depart elt) 1) (= (zone-depart elt) -1))
			 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
			 ;; Si la case adjacente est un départ ou une arriveé ;;
			 ;; et est de la même couleur que le chemin construit ;;
			 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
			 (= (zone-couleur elt) construit-couleur)))
	       ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	       ;; Alors on a un acces supplémentaire ;;
	       ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	       (incf N-acces))
	      
	      ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	      ;;Sinon la case-adjacente n'est pas un acces ;;
	      ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	      (t N-acces)))))
      N-acces)))


(defun nb-cases-vides (G)
  "Détermine le nombre de cases non coloriées"
  (let ((res 0)
	(A (grille-matrice G)))
    (dotimes (i (array-dimension A 0) res)
      (dotimes (j (array-dimension A 1))
	(when (null (zone-couleur (aref A i j)))
	  (incf res))))
    (setf (grille-nb-cases-vides G) res)))


(require "creation" "../src/creation-grille.lisp")
(require "primitives" "../src/primitives.lisp")
(require "affichage" "../src/affichage-ltk.lisp")
(require "resolution" "../src/algo-resolution.lisp")
(provide "tests-primitives")

(defun test-colorier-case (G)
  (format t " ~% Tests pour colorier et vider les cases de la grille ~%")
  (let ((tests 0)
	(succes 0))
    
    (charger-grille "../grille/5x5-0.txt" G)
    
    (incf tests)
    (when (and (null (colorier-case G 0 0 8))
	       (eq 1 (couleur G 0 0)))
      (incf succes))

    (incf tests)
    (when (and (null (colorier-case G 1 4 0))
	       (eq 1 (couleur G 0 0)))
      (incf succes))

    (incf tests)
    (when (and (null (vider-case G 4 0))
	       (eq 5 (couleur G 4 0)))
      (incf succes))

    (incf tests)
    (when (and (null (vider-case G 4 4))
	       (eq 5 (couleur G 4 4)))
      (incf succes))

    (charger-grille "../grille/5x5-0.txt" G)
    
    (incf tests)
    (colorier-case G 2 2 1)
    (when (and (= 1 (quelle-position G 0 0))
	       (= 2 (quelle-position G 2 2))
	       (= 3 (quelle-position G 1 4)))
      (incf succes))

    (incf tests)
    (colorier-case G 2 2 1)
    (when (and (= 1 (quelle-position G 0 0))
	       (= 2 (quelle-position G 2 2))
	       (= 3 (quelle-position G 1 4)))
      (incf succes))
    
    (incf tests)
    (setf (zone-couleur (obtenir-case G 4 2)) 5)
    (setf (zone-position (obtenir-case G 4 2)) 2)
    (setf (zone-position (obtenir-case G 4 4)) 3)
    (vider-case G 4 2)
    (when (and (= 1 (quelle-position G 4 0))
	       (= 0 (quelle-position G 4 2))
	       (= 2 (quelle-position G 4 4))
	       (= 0 (couleur G 4 2))
	       (= 5 (couleur G 4 0))
	       (= 5 (couleur G 4 4)))
      (incf succes))
  
  
    (format t "Nombre de tests réussis: ~D / ~D ~%"
	    succes
	    tests)))


(defun test-cases-adjacentes (G)
  (format t "~% Tests pour obtenir les cases adjacentes à la case étudiée~%")
  (let ((tests 0)
	(succes 0))
    
    (charger-grille "../grille/5x5-0.txt" G)
    
    (incf tests)
    (when (eq (case-gauche G 1 1) (obtenir-case G 1 0))
      (incf succes))

    (incf tests)
    (when (eq (case-droite G 1 1) (obtenir-case G 1 2))
      (incf succes))

    (incf tests)
    (when (eq (case-haut G 1 1) (obtenir-case G 0 1))
      (incf succes))
    
    (incf tests)
    (when (eq (case-bas G 1 1) (obtenir-case G 2 1))
      (incf succes))

    (incf tests)
    (when (and (null (case-haut G 0 0))
	       (null (case-gauche G 0 0))
	       (null (case-bas G 4 4))
	       (null (case-droite G 4 4)))
      (incf succes))
    
    (incf tests)
    (when (equal (cases-adjacentes G 1 1) (list (obtenir-case G 1 0)
					     (obtenir-case G 1 2)
					     (obtenir-case G 0 1)
					     (obtenir-case G 2 1)))
      (incf succes))
    
    (format t "Nombre de tests réussis: ~D / ~D ~%"
	    succes
	    tests)))


(defun test-trouver-depart (G)
  (format t "~% Tests pour trouver le point de départ ou d'arrivée d'une couleur donnée~%")
  (let ((tests 0)
	(succes 0))
    
    (charger-grille "../grille/5x5-0.txt" G)
    
    (incf tests)
    (when (null (trouver-depart G 0))
      (incf succes))

    (incf tests)
    (when (null (trouver-arrivee G 0))
      (incf succes))
    
    (incf tests)
    (when (eq (trouver-depart G 1) (obtenir-case G 0 0))
      (incf succes))
        
    (incf tests)
    (when (eq (trouver-arrivee G 1) (obtenir-case G 1 4))
      (incf succes))

    (charger-grille "../grille/sans-arrivee.txt" G)
    
    (incf tests)
    (when (null (trouver-arrivee G 1))
      (incf succes))

    (format t "Nombre de tests réussis: ~D / ~D ~%"
	    succes
	    tests)))


(defun test-case-suiv-prec (G)
  (format t "~% Tests pour savoir si une case est précédente ou suivante dans un chemine par rapport à la case étudiée~%")
  (let ((tests 0)
	(succes 0))
    
    (charger-grille "../grille/5x5-0.txt" G)
    
    (incf tests)
    (when (and (null (case-suivante G 0 0))
	       (null (case-suivante G 1 4)))
      (incf succes))
    
    
    (incf tests)
    (when (null (case-suivante G 0 1))
      (incf succes))
    
    (incf tests)
    (when (and (null (case-precedente G 1 4))
	       (null (case-precedente G 0 0)))
      (incf succes))

    (incf tests)
    (when (null (case-precedente G 2 2))
      (incf succes))
    
    (colorier-case G 0 1 1)
    (colorier-case G 0 2 1)
    (colorier-case G 0 3 1)
    (colorier-case G 0 4 1)
    
    (incf tests)
    (when (eq (case-suivante G 0 0) (obtenir-case G 0 1))
      (incf succes))

    (incf tests)
    (when (eq (case-suivante G 0 2) (obtenir-case G 0 3))
      (incf succes))

    (incf tests)
    (when (null (case-suivante G 1 4))
      (incf succes))

    (incf tests)
    (when (eq (case-precedente G 0 2) (obtenir-case G 0 1))
      (incf succes))

    (incf tests)
    (when (eq (case-precedente G 1 4) (obtenir-case G 0 4))
      (incf succes))

    (incf tests)
    (when (null (case-precedente G 0 0))
      (incf succes))

    (format t "Nombre de tests réussis: ~D / ~D ~%"
	    succes
	    tests)))


(defun test-dernier-construit (G)
  (format t "~% Tests pour savoir si une case est la dernière coloriée pour un chemin donnée:~%")
  (let ((tests 0)
	(succes 0))
    
    (charger-grille "../grille/5x5-0.txt" G)
    
    (colorier-case G 0 1 1)

    (incf tests)
    (when (null (est-dernier-construit G 0 0))
      (incf succes))
    
    (incf tests)
    (when (null (est-dernier-construit G 1 4))
      (incf succes))

    
    (incf tests)
    (when (est-dernier-construit G 0 1)
      (incf succes))

    (colorier-case G 0 2 1)
    (colorier-case G 0 3 1)
    (colorier-case G 0 4 1)

    (incf tests)
    (when (not (est-dernier-construit G 0 4))
      (incf succes))

    (incf tests)
    (when (not (est-dernier-construit G 1 4))
      (incf succes))

    (format t "Nombre de tests réussis: ~D / ~D ~%"
	    succes
	    tests)))


(defun test-primitives (G)
  (test-cases-adjacentes G)
  (test-trouver-depart G)
  (test-colorier-case G)
  (test-case-suiv-prec G)
  (test-dernier-construit G))



(defun tests-nb-acces (G)
  (format t "~% Tests de la fonction nb-acces ~%")
  (let ((tests 0)
	(succes 0))
    
    (charger-grille "../grille/5x5-0.txt" G)
    
    (incf tests)
    (when (= (nb-acces G 2 0) 2)
      (incf succes))
    
    
    (incf tests)
    (when (= (nb-acces G 2 2) 4)
      (incf succes))

    (incf tests)
    (when (= (nb-acces G 0 0) 1)
      (incf succes))

    (incf tests)
    (when (= (nb-acces G 0 2) 3)
      (incf succes))
    
    (colorier-case G 0 1 1)

    (incf tests)
    (when (= (nb-acces G 0 2) 3)
      (incf succes))

    (colorier-case G 0 2 1)
    
    (incf tests)
    (when (= (nb-acces G 0 1) 2)
      (incf succes))
    
    (colorier-case G 2 3 2)

    (incf tests)
    (when (= (nb-acces G 2 4) 1)
      (incf succes))
    
    (vider-case G 2 3)
    (colorier-case G 2 4 1)
    
    (incf tests)
    (when (= (nb-acces G 2 4) 1)
      (incf succes))
    
    (format t "Nombre de tests réussis: ~D / ~D ~%"
	    succes
	    tests)))




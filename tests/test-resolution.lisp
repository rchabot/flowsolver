(require "creation" "../src/creation-grille.lisp")
(require "primitives" "../src/primitives.lisp")
(require "resolution" "../src/algo-resolution.lisp")
(require "affichage" "../src/affichage-ltk.lisp")
(provide "tests-resolution")

(defun test-chemin-correct (G)
  (format t "~% Tests pour la fonction chemin-correct: ~%")
  (let ((tests 0)
	(succes 0))
    
    (charger-grille "../grille/5x5-0.txt" G)
    
    (incf tests)
    (when (null (chemin-correct G 0))
      (incf succes))
    
    (incf tests)
    (when (null (chemin-correct G 1))
      (incf succes))
    
    (colorier-case G 0 2 1)
    (colorier-case G 0 3 1)
    (colorier-case G 0 1 1)
    (colorier-case G 0 4 1)

    (incf tests)
    (when (null (chemin-correct G 1))
      (incf succes))

    (charger-grille "../grille/5x5-0.txt" G)

    (colorier-case G 0 1 1)
    (colorier-case G 0 2 1)
    (colorier-case G 0 3 1)
    (colorier-case G 0 4 1)

    (incf tests)
    (when (chemin-correct G 1)
      (incf succes))
    
    (vider-case G 0 1)
    (vider-case G 0 3)
    
    (incf tests)
    (when (null (chemin-correct G 1))
      (incf succes))

    
    (format t "Nombre de tests réussis: ~D / ~D ~%"
	    succes
	    tests)))


(defun test-est-resolue (G)
  (format t "~% Tests de la fonction est-resolue: ~%")
  (let ((tests 0)
	(succes 0))
    
    (charger-grille "../grille/4x4-0.txt" G)
    
    (incf tests)
    (when (null (est-resolue G))
      (incf succes))
    
    (colorier-case G 0 1 1)
    (colorier-case G 0 2 1)
    (colorier-case G 0 3 1)

    (colorier-case G 2 0 2)
    (colorier-case G 3 0 2)
    (colorier-case G 3 1 2)
    (colorier-case G 3 2 2)
    
    (incf tests)
    (when (null (est-resolue G))
      (incf succes))
    
    (charger-grille "../grille/5x5-0.txt" G)
    
    (colorier-case G 0 1 1)
    (colorier-case G 0 2 1)
    (colorier-case G 0 3 1)
    (colorier-case G 0 4 1)
    
    (incf tests)
    (when (not (est-resolue G))
      (incf succes))
    
    (colorier-case G 2 0 2)
    (colorier-case G 3 0 2)
    (colorier-case G 3 1 2)
    (colorier-case G 3 2 2)
    
    (colorier-case G 1 2 3)
    
    (colorier-case G 2 2 4)
    (colorier-case G 2 3 4)
    (colorier-case G 2 4 4)
    
    (colorier-case G 4 1 5)
    (colorier-case G 4 2 5)
    (colorier-case G 4 3 5)
    
    (incf tests)
    (when (est-resolue G)
      (incf succes))
    
    (vider-case G 4 1)
    (vider-case G 4 2)
    (colorier-case G 4 2 5)
    (colorier-case G 4 1 5)

    (incf tests)
    (when (not (est-resolue G))
      (incf succes))

    (format t "Nombre de tests réussis: ~D / ~D ~%"
	    succes
	    tests)))
(defun tests-cases-adjacentes-libres (G)
  (format t "~% Tests pour la fonction récupérant les cases accessibles à la case en cours lors de la résoltuion~%")
  (let ((tests 0)
	(succes 0))
    
    (charger-grille "../grille/5x5-0.txt" G)
    
    (incf tests)
    (when (equal (list (obtenir-case G 0 1)) 
		 (cases-adjacentes-libres G 
					  (obtenir-case G 0 0) 
					  (grille-case-nulle G)))
      (incf succes))
    
    (incf tests)
    (when (equal (list (obtenir-case G 0 1)) 
		 (ordonner-liste G (cases-adjacentes-libres G 
							    (obtenir-case G 0 0) 
							    (grille-case-nulle G))
				 (obtenir-case G 0 0)))
      (incf succes))
    
    
    (incf tests)
    (when (equal (list (obtenir-case G 0 1) (obtenir-case G 0 3) (obtenir-case G 1 2)) 
		 (cases-adjacentes-libres G 
					  (obtenir-case G 0 2) 
					  (grille-case-nulle G)))
      (incf succes))

    
    (incf tests)
    (when (equal (list (obtenir-case G 0 1) (obtenir-case G 0 3) (obtenir-case G 1 2)) 
		 (ordonner-liste G
				 (cases-adjacentes-libres G 
							  (obtenir-case G 0 2) 
							  (grille-case-nulle G)) 
				 (obtenir-case G 0 2)))
      (incf succes))
    
        
    (incf tests)
    (when (equal (list (obtenir-case G 1 2) (obtenir-case G 0 1))  
		 (cases-adjacentes-libres G 
					  (obtenir-case G 1 1) 
					  (grille-case-nulle G)))
      (incf succes))

    (colorier-case G 0 4 1)
    (incf tests)
    (when (equal (list (obtenir-case G 1 4))  
		 (cases-adjacentes-libres G 
					  (obtenir-case G 0 4) 
					  (obtenir-case G 0 3)))
      (incf succes))
    
    (colorier-case G 2 0 3)
    (colorier-case G 3 0 3)
    (incf tests)
    (when (null (cases-adjacentes-libres G 
					 (obtenir-case G 2 0) 
					 (obtenir-case G 3 0)))
      (incf succes))
    
    (vider-case G 0 2)
    (vider-case G 2 2)
    (colorier-case G 1 2 3)
    (incf tests)
    (when (equal (list (obtenir-case G 2 2) 
		       (obtenir-case G 0 2) 
		       (obtenir-case G 1 3)) 
		 (ordonner-liste G 
				 (cases-adjacentes-libres G 
							  (obtenir-case G 1 2) 
							  (obtenir-case G 1 1))
				 (obtenir-case G 1 2)))
      
      (incf succes))

    (format t "Nombre de tests réussis: ~D / ~D ~%"
	    succes
	    tests)))


(defun tests-rebrousser-chemin (G)
  (format t "~%Tests de la fonction de dépilage dans l'algo de résolution~%")
  
  (let ((tests 0)
	(succes 0)
	(P nil))
    
    (charger-grille "../grille/5x5-0.txt" G)
    
    ;; Cas où P est entièrement dépilée
    (setf P (empiler-depart G 1 P))
    (push (make-chemin :zone (obtenir-case G 0 1)
		       :precedente (obtenir-case G 0 0))
	  P)
    (colorier-case G 0 1 1)
    
    (push (make-chemin :zone (obtenir-case G 0 2)
		       :precedente (obtenir-case G 0 1))
	  P)
    (colorier-case G 0 2 1)
    
    (setf P (rebrousser-chemin G P))

    (incf tests)
    (when (and (null P)
	       (= (couleur G 0 0) 1))
      (incf succes))

    (incf tests)
    (when (and (= (couleur G 0 1) 0)
	       (= (couleur G 0 2) 0))
      (incf succes))

    ;; Cas où on revient en arrière jusqu'à une case non explorée
    (setf P (empiler-depart G 1 P))
    (push (make-chemin :zone (obtenir-case G 0 1)
		       :precedente (obtenir-case G 0 0))
	  P)
    (colorier-case G 0 1 1)
    
    (push (make-chemin :zone (obtenir-case G 0 2)
		       :precedente (obtenir-case G 0 1))
	  P)
    (colorier-case G 0 2 1)
    
    (push (make-chemin :zone (obtenir-case G 0 3)
		       :precedente (obtenir-case G 0 2))
	  P)
    
    (push (make-chemin :zone (obtenir-case G 1 2)
		       :precedente (obtenir-case G 0 2))
	  P)
    (colorier-case G 1 2 1)
    
    (push (make-chemin :zone (obtenir-case G 2 2)
		       :precedente (obtenir-case G 2 2))
	  P)
    (colorier-case G 2 2 1)
    
    (setf P (rebrousser-chemin G P))
    (incf tests)
    (when (and (eq (chemin-zone (car P)) (obtenir-case G 0 3))
	       (= 1 (couleur G 0 3)))
      (incf succes))
    
    ;; Cas où on dépile jusqu'à la couleur précédente
    (charger-grille "../grille/5x5-0.txt" G)
    (push (make-chemin :zone (obtenir-case G 0 1)
		       :precedente (obtenir-case G 0 0))
	  P)
    (colorier-case G 0 1 1)
    
    (push (make-chemin :zone (obtenir-case G 0 2)
		       :precedente (obtenir-case G 0 1))
	  P)
    (colorier-case G 0 2 1)
    
    (push (make-chemin :zone (obtenir-case G 0 3)
		       :precedente (obtenir-case G 0 2))
	  P)
    
    (push (make-chemin :zone (obtenir-case G 1 2)
		       :precedente (obtenir-case G 0 2))
	  P)
    (colorier-case G 1 2 1)
    
    (push (make-chemin :zone (obtenir-case G 2 2)
		       :precedente (obtenir-case G 1 2))
	  P)
    (colorier-case G 2 2 1)
    
    (push (make-chemin :zone (obtenir-case G 2 3)
		       :precedente (obtenir-case G 2 2))
	  P)
    (colorier-case G 2 3 1)
    
    
    (push (make-chemin :zone (obtenir-case G 2 4)
		       :precedente (obtenir-case G 2 3))
	  P)
    (colorier-case G 2 4 1)
    
    (setf P (empiler-depart G 2 P))
    
    (push (make-chemin :zone (obtenir-case G 2 0)
		       :precedente (obtenir-case G 1 0))
	  P)
    (colorier-case G 2 0 2)
    ;(affichage-pile P)
    ;(affichage-grille G)
    (setf P (rebrousser-chemin G P))
    (incf tests)
    (when (eq (chemin-zone (car P)) (obtenir-case G 0 3))
      (incf succes))

    (incf tests)
    (when (null (rebrousser-chemin G nil))
      (incf succes))

  (format t "Nombre de tests réussis: ~D / ~D ~%"
	  succes
	  tests)))

(defun tests-construire-chemin (G)
  (format t "~% Tests de la fonction construire-chemin~%")
  (let ((tests 0)
	(succes 0)
	(P nil))
    
    (charger-grille "../grille/5x5-0.txt" G)

    (incf tests)
    (when (null (construire-chemin G nil))
      (incf succes))
    
    (setf P (empiler-depart G 1 P))
    (setf P (construire-chemin G P))
    (incf tests)
    (when (chemin-correct G 1)
      (incf succes))

    (setf P (rebrousser-chemin G P))
    (setf P (construire-chemin G P))
    (affichage-grille G)
    (incf tests)
    (when (chemin-correct G 1)
      (incf succes))
    
    (colorier-case G 3 0 3)
    (setf P (empiler-depart G 2 P))
    (setf P (construire-chemin G P))
    (incf tests)
    (when (null P)
      (incf succes))
    
    (format t "Nombre de tests réussis: ~D / ~D ~%"
	    succes
	    tests)))


(defun test-resolution (G)
  (test-chemin-correct G)
  (test-est-resolue G)
  (tests-cases-adjacentes-libres G)
  (tests-rebrousser-chemin G))
